#!/bin/sh
set -e

case "${DEBUG}" in
(1|true|y|yes|Yes|YES|debug) set -x;;
esac

: "ENV=${ENV}"

##
# Default settings if not specified by Docker
DB_PORT="${DB_PORT:=8080}"

##
# Required settings
for var in DB_ENDPOINT DB_USERNAME DB_PASSWORD DB_NAME; do
	eval "test_var=\"\$${var}\""
	if test -z "${test_var}"; then
		printf "!!!!!!!!!!!\nERROR: '%s' is not set" "${var}" >&2
		exit 1
	fi
done

##
# Update config
sed -i \
	-e "s/@DB_ENDPOINT@/${DB_ENDPOINT}/g" \
	-e "s/@DB_USERNAME@/${DB_USERNAME}/g" \
	-e "s/@DB_PASSWORD@/${DB_PASSWORD}/g" \
	-e "s/@DB_NAME@/${DB_NAME}/g" \
	-e "s/@DB_PORT@/${DB_PORT}/g" \
	/app/settings.yml

##
# Exec our CMD
exec "${@}"
