FROM openjdk:7-alpine

ADD service-1.0-SNAPSHOT.jar settings.yml /app/
COPY entrypoint.sh /
RUN set -ex && \
	chmod a+rx /entrypoint.sh

EXPOSE 8080

ENTRYPOINT ["/entrypoint.sh"]
CMD ["java", "-jar", "/app/server.jar", "server", "/app/settings.yml"]
